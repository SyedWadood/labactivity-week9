#include <iostream>
#include "numberconversion.h"
#include<string> 
using namespace std;

int value(char c)
{
  
  if(c == 'I')
    return 1;
  if(c == 'V')
    return 5;
  if(c == 'X')
    return 10;
  if(c == 'L')
    return 50;
  if(c == 'C')
    return 100;
  if(c == 'D')
    return 500;
  if(c == 'M')
    return 1000;

  return NULL;
}

int romantoint(string roman_numeral)
{
  int num_result = 0;

  for(int i = 0; i <= roman_numeral.length(); i++)
    {
      int placevalue1 = value(roman_numeral[i]);

      if(i + 1 < roman_numeral.length())
	{
	  int placevalue2 = value(roman_numeral[i+1]);

	  if(placevalue1 >= placevalue2)
	    {
	      num_result = num_result + placevalue1;
	    }
	  else
	    {
	      num_result = num_result +(placevalue2 - placevalue1);
	      i++;
	    }
	}
      else
	{
	  num_result = num_result + placevalue1;
	}
    }
  return num_result;
}

string inttoroman(int number)
{
  {
    int num[] = {1,4,5,9,10,40,50,90,100,400,500,900,1000};
    string sym[] = {"I","IV","V","IX","X","XL","L","XC","C","CD","D","CM","M"};
    int i=12;    
    while(number>0)
    {
      int div = number/num[i];
      number = number%num[i];
      while(div--)
      {
	      cout<<sym[i];
      }
      i--;
    }
  return "";
}
  
}
